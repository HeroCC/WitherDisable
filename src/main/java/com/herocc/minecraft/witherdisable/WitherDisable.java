package com.herocc.minecraft.witherdisable;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.UUID;

public final class WitherDisable extends JavaPlugin implements Listener, CommandExecutor {
  public ArrayList<UUID> immunePlayers = new ArrayList<>();

  @Override
  public void onEnable() {
    this.getServer().getPluginManager().registerEvents(this, this);
    this.getCommand("bubble").setExecutor(this);
  }

  @EventHandler
  public void entityDamageCheck(EntityDamageEvent e) {
    if (!(e.getEntity() instanceof Player)) { return; } // If it isn't a player

    if (e.getCause() == EntityDamageEvent.DamageCause.WITHER) {
      Player p = (Player) e.getEntity();
      if (immunePlayers.contains(p.getUniqueId())) { // If player is immune
        e.setCancelled(true);
      }
    }
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("The Console will forever be immune to the effects of the wither!");
    } else {
      Player p = (Player) sender;
      if (p.hasPermission("witherblock.bubble")) {
        p.sendMessage(ChatColor.GREEN + "You are immune to the effects of the wither for 3 seconds!");
        immunePlayers.add(p.getUniqueId()); // Add the player to the immune list
        this.getServer().getScheduler().scheduleSyncDelayedTask(this, () ->
            immunePlayers.remove(p.getUniqueId()), 100L); // After 100 ticks (5 seconds), remove from list
      } else {
        p.sendMessage(ChatColor.RED + "Silly, you can't be immune to the wither!");
      }
    }
    return true;
  }
}
